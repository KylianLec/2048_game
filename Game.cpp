#include "Game.h"

Game::Game()
{
	this->board = nullptr;
	this->window = nullptr;

	this->initWindow();
	this->initBoard();
}

void Game::initWindow()
{
	this->window = new sf::RenderWindow(sf::VideoMode(_WINDOW_WIDTH_, _WINDOW_HEIGHT_), "2048");
	this->window->setFramerateLimit(_FPS_LIMIT_);
}

void Game::initBoard()
{
	this->board = new Board();
}

void Game::run()
{
	while (this->window->isOpen()) {
		sf::Event event;

		if (this->board->getQuit())
			this->window->close();

		if (this->board->getReplay())
		{
			this->board = new Board();
		}

		while (this->window->pollEvent(event)) {
			switch (event.type)
			{
			case sf::Event::Closed:
				this->window->close();
				break;
			case sf::Event::KeyPressed:
				if (!this->keyIsPressed && !this->board->getLose())
				{
					this->board->move(event.key.code);
					this->board->generateNewCase();
				}
				if (event.key.code == sf::Keyboard::Up ||
					event.key.code == sf::Keyboard::Down ||
					event.key.code == sf::Keyboard::Left ||
					event.key.code == sf::Keyboard::Right)
				{
					this->keyIsPressed = true;
				}
				break;
			case sf::Event::KeyReleased:
				if (event.key.code == sf::Keyboard::Up ||
					event.key.code == sf::Keyboard::Down ||
					event.key.code == sf::Keyboard::Left ||
					event.key.code == sf::Keyboard::Right)
				{
					this->keyIsPressed = false;
				}
				
				break;
			}
		}

		this->update();
		this->render();
	}
}

void Game::update()
{
	// Update the game
	this->board->update(sf::Mouse::getPosition(*this->window));
}

void Game::render()
{
	// Erase content of the window
	this->window->clear();

	// Draw gui
	this->board->render(this->window);

	// Display in the window
	this->window->display();
}