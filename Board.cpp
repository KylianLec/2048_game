
#include "Board.h"

Board::Board()
{
	srand(time(NULL));
	
	this->initBoard();
	this->initGame();
}

void Board::initBoard()
{
	// Create title
	this->font = new sf::Font();
	this->font->loadFromFile("./Fonts/Roboto-Bold.ttf");
	this->title = sf::Text("2048", *font, 50);
	this->title.setPosition(20, 20);
	this->title.setFillColor(sf::Color::White);

	// Create score rect shape display
	this->scoreRect.setSize(sf::Vector2f(_SCORE_RECT_WIDTH_, _SCORE_RECT_HEIGHT_));
	this->scoreRect.setPosition(sf::Vector2f(200, 30));
	this->scoreRect.setFillColor(_BOARD_COLOR_);
	// Create score text display
	this->scoreTxt = sf::Text(std::to_string(this->score), *this->font, 30);
	this->scoreTxt.setPosition(
		(this->scoreRect.getPosition().x + (this->scoreRect.getGlobalBounds().width / 2.f) - this->scoreRect.getGlobalBounds().width / 2.f)+10,
		this->scoreRect.getPosition().y + (this->scoreRect.getGlobalBounds().height / 2.f) - this->scoreRect.getGlobalBounds().height / 2.f
	);
	this->scoreTxt.setFillColor(sf::Color::White);

	// Create high score display
	this->highScoreRect.setSize(sf::Vector2f(_SCORE_RECT_WIDTH_, _SCORE_RECT_HEIGHT_));
	this->highScoreRect.setPosition(sf::Vector2f(_WINDOW_WIDTH_-(_START_END_SPACE_/2)- _SCORE_RECT_WIDTH_, 30));
	this->highScoreRect.setFillColor(_BOARD_COLOR_);
	// Create score text display
	this->highScoreTxt = sf::Text(std::to_string(this->score), *this->font, 30);
	this->highScoreTxt.setPosition(
		(this->highScoreRect.getPosition().x + (this->highScoreRect.getGlobalBounds().width / 2.f) - this->highScoreRect.getGlobalBounds().width / 2.f)+10,
		this->highScoreRect.getPosition().y + (this->highScoreRect.getGlobalBounds().height / 2.f) - this->highScoreRect.getGlobalBounds().height / 2.f
	);
	this->highScoreTxt.setFillColor(sf::Color::White);

	// Create board shape
	this->boardShape.setSize(sf::Vector2f(_BOARD_WIDTH_, _BOARD_HEIGHT_));
	this->boardShape.setPosition(sf::Vector2f(_BOARD_X_, _BOARD_Y_));
	this->boardShape.setFillColor(_BOARD_COLOR_);

	// Create cases shapes
	for (uint8 x = 0; x < _NB_CASE_LINE_; x++)
	{
		std::vector< sf::RectangleShape> line;
		for (uint8 y = 0; y < _NB_CASE_LINE_; y++)
		{
			sf::RectangleShape caseShape;
			caseShape.setPosition(sf::Vector2f(
				_START_END_SPACE_ + x * _SPACE_SIZE_ + x * _CASE_SIZE_,
				_HEADER_ + _START_END_SPACE_ + y * _SPACE_SIZE_ + y * _CASE_SIZE_));
			caseShape.setSize(sf::Vector2f(_CASE_SIZE_, _CASE_SIZE_));
			caseShape.setFillColor(_BASIC_CASE_COLOR_);
			line.push_back(caseShape);
		}
		this->caseShapes.push_back(line);
	}

	// Init 2D list of cases to nullptr 
	for (uint8 x = 0; x < _NB_CASE_LINE_; x++)
	{
		std::vector<Case*> line;
		for (uint8 y = 0; y < _NB_CASE_LINE_; y++)
		{
			line.push_back(nullptr);
		}
		this->cases.push_back(line);
	}

	// Create end menu
	this->endRect.setSize(sf::Vector2f(299, 130));
	this->endRect.setPosition(sf::Vector2f(106, 270));
	this->endRect.setFillColor(sf::Color(255, 255, 255, 190));

	// Create score text display
	this->endText = sf::Text("Score : " + std::to_string(this->score), *this->font, 30);
	this->endText.setPosition(sf::Vector2f(175, 280));
	this->endText.setFillColor(sf::Color(112, 112, 112));

	// Create replay button
	this->replayButtonTxt = sf::Text("Rejouer", *this->font, 30);
	this->replayButtonTxt.setPosition(136, 344);
	this->replayButtonTxt.setFillColor(sf::Color(112, 112, 112));

	// Create quit button
	this->quitButtonTxt = sf::Text("Quitter", *this->font, 30);
	this->quitButtonTxt.setPosition(282, 344);
	this->quitButtonTxt.setFillColor(sf::Color(112, 112, 112));
}

void Board::initGame()
{
	this->lose = false;
	this->nbEmptyCase = _NB_CASE_;
	this->quit = false;
	this->replay = false;
	this->caseIsMoving = true;

	this->generateNewCase();
	this->generateNewCase();
}

void Board::move(sf::Keyboard::Key key)
{
	this->caseIsMoving = false;
	switch (key)
	{
	case sf::Keyboard::Left:
		for (uint8 x = 0; x < _NB_CASE_LINE_; ++x)
		{
			for (uint8 y = 0; y < _NB_CASE_LINE_; ++y)
			{
				if (cases[x][y])
				{
					Case* tmp_case = this->cases[x][y];
					this->cases[x][y] = nullptr;
					uint8 curr_x = x;
					while (curr_x - 1 >= 0 && this->cases[curr_x - 1][y] == nullptr)
					{
						if (curr_x - 1 >= 0)
							curr_x = curr_x - 1;
					}

					bool addition = false;
					if (curr_x - 1 >= 0 && this->cases[curr_x - 1][y] != nullptr)
					{
						if (tmp_case->getValue() == this->cases[curr_x - 1][y]->getValue())
						{
							addition = true;
							this->cases[curr_x - 1][y] = tmp_case;
							this->cases[curr_x - 1][y]->setPosX(curr_x - 1);
							this->cases[curr_x - 1][y]->setValue(this->cases[curr_x - 1][y]->getValue() * 2);
							this->score += this->cases[curr_x - 1][y]->getValue();
							this->nbEmptyCase++;
							this->caseIsMoving = true;
						}
					}
					
					if (!addition)
					{
						if (tmp_case->getPosX() != curr_x || tmp_case->getPosY() != y)
							this->caseIsMoving = true;
						this->cases[curr_x][y] = tmp_case;
						this->cases[curr_x][y]->setPosX(curr_x);
					}
				}
			}
		}
		break;
	case sf::Keyboard::Right:
		for (int8 x = _NB_CASE_LINE_ - 1; x >= 0; --x)
		{
			for (uint8 y = 0; y < _NB_CASE_LINE_; ++y)
			{
				if (cases[x][y])
				{
					Case* tmp_case = this->cases[x][y];
					this->cases[x][y] = nullptr;
					uint8 curr_x = x;
					while (curr_x + 1 < _NB_CASE_LINE_ && this->cases[curr_x + 1][y] == nullptr)
					{
						if (curr_x + 1 < _NB_CASE_LINE_)
							curr_x = curr_x + 1;
					}

					bool addition = false;
					if (curr_x + 1 < _NB_CASE_LINE_ && this->cases[curr_x + 1][y] != nullptr)
					{
						if (tmp_case->getValue() == this->cases[curr_x + 1][y]->getValue())
						{
							addition = true;
							this->cases[curr_x + 1][y] = tmp_case;
							this->cases[curr_x + 1][y]->setPosX(curr_x + 1);
							this->cases[curr_x + 1][y]->setValue(this->cases[curr_x + 1][y]->getValue() * 2);
							this->score += this->cases[curr_x + 1][y]->getValue();
							this->nbEmptyCase++;
							this->caseIsMoving = true;
						}
					}

					if (!addition)
					{
						if (tmp_case->getPosX() != curr_x || tmp_case->getPosY() != y)
							this->caseIsMoving = true;
						this->cases[curr_x][y] = tmp_case;
						this->cases[curr_x][y]->setPosX(curr_x);
					}
				}
			}
		}
		break;
	case sf::Keyboard::Up:
		for (uint8 x = 0; x < _NB_CASE_LINE_; ++x)
		{
			for (uint8 y = 0; y < _NB_CASE_LINE_; ++y)
			{
				if (cases[x][y])
				{
					Case* tmp_case = this->cases[x][y];
					this->cases[x][y] = nullptr;
					uint8 curr_y = y;
					while (curr_y - 1 >= 0 && this->cases[x][curr_y - 1] == nullptr)
					{
						if (curr_y - 1 >= 0)
							curr_y = curr_y - 1;
					}

					bool addition = false;
					if (curr_y - 1 >= 0 && this->cases[x][curr_y - 1] != nullptr)
					{
						if (tmp_case->getValue() == this->cases[x][curr_y - 1]->getValue())
						{
							addition = true;
							this->cases[x][curr_y - 1] = tmp_case;
							this->cases[x][curr_y - 1]->setPosY(curr_y - 1);
							this->cases[x][curr_y - 1]->setValue(this->cases[x][curr_y - 1]->getValue() * 2);
							this->score += this->cases[x][curr_y - 1]->getValue();
							this->nbEmptyCase++;
							this->caseIsMoving = true;
						}
					}
					
					if (!addition)
					{
						if (tmp_case->getPosX() != x || tmp_case->getPosY() != curr_y)
							this->caseIsMoving = true;
						this->cases[x][curr_y] = tmp_case;
						this->cases[x][curr_y]->setPosY(curr_y);
					}
				}
			}
		}
		break;
	case sf::Keyboard::Down:
		for (uint8 x = 0; x < _NB_CASE_LINE_; ++x)
		{
			for (int8 y = _NB_CASE_LINE_-1; y >= 0; --y)
			{
				if (cases[x][y] && y < _NB_CASE_LINE_ - 1)
				{
					Case* tmp_case = this->cases[x][y];
					this->cases[x][y] = nullptr;
					uint8 curr_y = y;
					while (curr_y + 1 < _NB_CASE_LINE_ && this->cases[x][curr_y + 1] == nullptr)
					{
						if (curr_y + 1 < _NB_CASE_LINE_)
							curr_y = curr_y + 1;
					}

					bool addition = false;
					if (curr_y + 1 < _NB_CASE_LINE_ && this->cases[x][curr_y + 1] != nullptr)
					{
						if (tmp_case->getValue() == this->cases[x][curr_y + 1]->getValue())
						{
							addition = true;
							this->cases[x][curr_y + 1] = tmp_case;
							this->cases[x][curr_y + 1]->setPosY(curr_y + 1);
							this->cases[x][curr_y + 1]->setValue(this->cases[x][curr_y + 1]->getValue() * 2);
							this->score += this->cases[x][curr_y + 1]->getValue();
							this->nbEmptyCase++;
							this->caseIsMoving = true;
						}
					}

					if (!addition)
					{
						if (tmp_case->getPosX() != x || tmp_case->getPosY() != curr_y)
							this->caseIsMoving = true;
						this->cases[x][curr_y] = tmp_case;
						this->cases[x][curr_y]->setPosY(curr_y);
					}
				}
			}
		}
		break;
	}
}

void Board::update(const sf::Vector2i& mousePos)
{
	for (const auto& line : this->cases)
		for (const auto& ele : line)
			if (ele)
				ele->update();

	this->scoreTxt.setString(std::to_string(this->score));
	this->highScoreTxt.setString(std::to_string(this->score));

	if (this->lose)
	{
		this->endText.setString("Score : " + std::to_string(this->score));

		this->relayButton = button_states::BTN_IDLE;
		this->quitButton = button_states::BTN_IDLE;

		// Update replay button
		if (this->replayButtonTxt.getGlobalBounds().contains(static_cast<sf::Vector2f>(mousePos)))
		{
			this->relayButton = button_states::BTN_HOVER;

			if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
			{
				this->relayButton = button_states::BTN_ACTIVE;
			}
		}

		switch (this->relayButton)
		{
		case button_states::BTN_IDLE:
			this->replayButtonTxt.setCharacterSize(30);
			this->replayButtonTxt.setPosition(136, 344);
			this->replayButtonTxt.setFillColor(sf::Color(112, 112, 112));
			break;
		case button_states::BTN_HOVER:
			this->replayButtonTxt.setCharacterSize(32);
			this->replayButtonTxt.setPosition(136, 342);
			this->replayButtonTxt.setFillColor(sf::Color(120, 120, 120));
			break;
		case button_states::BTN_ACTIVE:
			this->replayButtonTxt.setCharacterSize(32);
			this->replayButtonTxt.setPosition(136, 342);
			this->replayButtonTxt.setFillColor(sf::Color(140, 140, 140));
			this->replay = true;
			break;
		}

		if (this->quitButtonTxt.getGlobalBounds().contains(static_cast<sf::Vector2f>(mousePos)))
		{
			this->quitButton = button_states::BTN_HOVER;

			if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
			{
				this->quitButton = button_states::BTN_ACTIVE;
			}
		}

		switch (this->quitButton)
		{
		case button_states::BTN_IDLE:
			this->quitButtonTxt.setCharacterSize(30);
			this->quitButtonTxt.setPosition(282, 344);
			this->quitButtonTxt.setFillColor(sf::Color(112, 112, 112));
			break;
		case button_states::BTN_HOVER:
			this->quitButtonTxt.setCharacterSize(32);
			this->quitButtonTxt.setPosition(282, 342);
			this->quitButtonTxt.setFillColor(sf::Color(120, 120, 120));
			break;
		case button_states::BTN_ACTIVE:
			this->quitButtonTxt.setCharacterSize(32);
			this->quitButtonTxt.setPosition(282, 342);
			this->quitButtonTxt.setFillColor(sf::Color(140, 140, 140));
			this->quit = true;
			break;
		}
	}
}

void Board::render(sf::RenderWindow *window)
{
	window->draw(this->title);

	window->draw(this->scoreRect);
	window->draw(this->scoreTxt);
	window->draw(this->highScoreRect);
	window->draw(this->highScoreTxt);
	
	window->draw(this->boardShape);

	for (uint8 i = 0; i < _NB_CASE_LINE_; i++)
	{
		for (uint8 j = 0; j < _NB_CASE_LINE_; j++)
		{
			window->draw(this->caseShapes[i][j]);
		}
	}

	for (const auto& line : this->cases)
		for (const auto& currCase : line)
			if (currCase)
				currCase->draw(window);
	
	if (this->lose)
	{
		window->draw(this->endRect);
		window->draw(this->endText);
		window->draw(this->replayButtonTxt);
		window->draw(this->quitButtonTxt);
	}
}

void Board::generateNewCase()
{
	if (this->nbEmptyCase > 0 && this->caseIsMoving)
	{
		uint8 x = 0;
		uint8 y = 0;
		do {
			x = rand() % 4;
			y = rand() % 4;
		} while (this->cases[x][y] != nullptr);

		const uint8 randValue = rand() % 11;
		uint8 value = 4;
		if (randValue > 3) value = 2;
		
		this->cases[x][y] = new Case(x, y, this->font, value);
		this->nbEmptyCase--;

		this->lose = this->gameIsDone();
	}
}

bool Board::gameIsDone()
{
	if (this->nbEmptyCase == 0)
	{
		for (uint8 x = 0; x < _NB_CASE_LINE_; ++x)
		{
			for (uint8 y = 0; y < _NB_CASE_LINE_; ++y)
			{
				if (x + 1 < _NB_CASE_LINE_)
				{
					if (this->cases[x + 1][y])
					{
						if (this->cases[x][y]->getValue() == this->cases[x + 1][y]->getValue())
						{
							return false;
						}
					}
					else return false;
				}

				if (x - 1 >= 0)
				{
					if (this->cases[x - 1][y])
					{
						if (this->cases[x][y]->getValue() == this->cases[x - 1][y]->getValue())
						{ 
							return false;
						}
					}
					else return false;
				}

				if (y + 1 < _NB_CASE_LINE_)
				{
					if (this->cases[x][y + 1])
					{
						if (this->cases[x][y]->getValue() == this->cases[x][y + 1]->getValue())
						{
							return false;
						}
					}
					else return false;
				}

				if (y - 1 >= 0)
				{
					if (this->cases[x][y - 1])
					{
						if (this->cases[x][y]->getValue() == this->cases[x][y - 1]->getValue())
						{
							return false;
						}
					}
					else return false; 
				}

				// Last checked case
				if (x == 3 && y == 3) return true;
			}
		}
	}
	else return false;
}