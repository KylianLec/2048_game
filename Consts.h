#ifndef DEF_CONSTANTE
#define DEF_CONSTANTE
#pragma once

#include <SFML/Graphics.hpp>

typedef unsigned __int16 uint16;
typedef __int16 int16;
typedef unsigned __int8 uint8;
typedef __int8 int8;

const uint16 _WINDOW_WIDTH_(510);
const uint16 _WINDOW_HEIGHT_(710);

const uint8 _FPS_LIMIT_(60);

const uint8 _BOARD_X_(20);
const uint8 _BOARD_Y_(220);

const uint16 _BOARD_WIDTH_(470);
const uint16 _BOARD_HEIGHT_(470);

const sf::Color _BOARD_COLOR_(sf::Color(187, 173, 160));
const sf::Color _BASIC_CASE_COLOR_(sf::Color(205, 193, 180));

const uint8 _NB_CASE_(16);
const uint8 _NB_CASE_LINE_(4);

const uint8 _CASE_SIZE_(100);
const uint8 _SPACE_SIZE_(10);
const uint8 _START_END_SPACE_(40);
const uint8 _HEADER_(200);

const uint8 _SCORE_RECT_WIDTH_(130);
const uint8 _SCORE_RECT_HEIGHT_(40);

#endif

/*
unsigned __int8 = unsigned char = 0 255
unsigned __int16 = unsigned short = 0 � 65 535
*/