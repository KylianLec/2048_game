#include <stdlib.h>
#include <time.h>
#include "Case.h"
#include "Consts.h"

enum class button_states{BTN_IDLE = 0, BTN_HOVER, BTN_ACTIVE};

class Board
{

public:
	Board();

	void initBoard();
	void initGame();

	void move(sf::Keyboard::Key key);
	void update(const sf::Vector2i& mousePos);
	void render(sf::RenderWindow *window);

	void generateNewCase();
	bool gameIsDone();

	inline bool getLose() const { return this->lose; };
	inline bool getQuit() const { return this->quit; };
	inline bool getReplay() const { return this->replay; };

private:
	sf::Font* font;

	sf::Text title;

	sf::RectangleShape scoreRect;
	sf::Text scoreTxt;
	
	sf::RectangleShape highScoreRect;
	sf::Text highScoreTxt;

	sf::RectangleShape boardShape;
	std::vector< std::vector< sf::RectangleShape> > caseShapes;

	std::vector< std::vector< Case*> > cases;

	bool caseIsMoving;
	int score;
	uint8 nbEmptyCase;
	bool lose;

	sf::RectangleShape endRect;
	sf::Text endText;

	sf::Text replayButtonTxt;
	button_states relayButton;

	sf::Text quitButtonTxt;
	button_states quitButton;

	bool quit;
	bool replay;
};
