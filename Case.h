#include <iostream>
#include "Consts.h"

class Case
{
public:
	Case(float x, float y, sf::Font *font, int val = 2);

	void update();
	void draw(sf::RenderWindow* window);

	inline uint8 getPosX() const { return posX; };
	inline uint8 getPosY() const { return posY; };
	inline unsigned int getValue() const { return value; };
	inline sf::RectangleShape& getShape() { return shape; };
	inline sf::Text& getText() { return text; };
	sf::Color getColor(unsigned int value);

	inline void setPosX(uint8 x) { posX = x; };
	inline void setPosY(uint8 y) { posY = y; };
	inline void setValue(unsigned int value) { value = value; };

private:
	sf::RectangleShape shape;

	uint8 posX;
	uint8 posY;
	sf::Font* font;
	sf::Text text;
	unsigned int value;
	const sf::Color CLR_VOID_CASE = sf::Color(205, 193, 180);
};
