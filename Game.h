#include <iostream>
#include <SFML/Graphics.hpp>
#include "Board.h"

#pragma once
class Game
{
public:
	Game();

	void run();
	void update();
	void render();

private:
	sf::RenderWindow *window;
	Board* board;

	bool keyIsPressed;

	void initWindow();
	void initBoard();
};

