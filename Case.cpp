#include "Case.h"

Case::Case(float x, float y, sf::Font *font, int val) :
	posX(x), posY(y), font(font), value(val)
{
	this->shape.setSize(sf::Vector2f(_CASE_SIZE_, _CASE_SIZE_));
	this->shape.setPosition(sf::Vector2f(
		_START_END_SPACE_ + x * _SPACE_SIZE_ + x * _CASE_SIZE_,
		_HEADER_ + _START_END_SPACE_ + y * _SPACE_SIZE_ + y * _CASE_SIZE_));
	this->shape.setFillColor(getColor(value));

	//sf::Font ffont = sf::Font();
	//ffont.loadFromFile("./Fonts/Roboto-Bold.ttf");
	this->text = sf::Text(std::to_string(value), *this->font, 40);
	this->text.setStyle(sf::Text::Bold);
	this->text.setFillColor(sf::Color(239, 231, 226));
}

void Case::update()
{
	this->shape.setFillColor(getColor(value));
	this->shape.setPosition(sf::Vector2f(
		_START_END_SPACE_ + this->posX * _SPACE_SIZE_ + this->posX * _CASE_SIZE_,
		_HEADER_ + _START_END_SPACE_ + this->posY * _SPACE_SIZE_ + this->posY * _CASE_SIZE_));
	this->text.setPosition(
		(this->shape.getPosition().x + (this->shape.getGlobalBounds().width / 2.f) - this->text.getGlobalBounds().width / 2.f) - 2,
		(this->shape.getPosition().y + (this->shape.getGlobalBounds().height / 2.f) - this->text.getGlobalBounds().height / 2.f) - 10
	);
	this->text.setString(std::to_string(value));
	if (this->value > 10 && this->value < 100)
		this->text.setCharacterSize(38);
	else if (this->value > 100 && this->value < 1000)
		this->text.setCharacterSize(36);
	else if (this->value > 1000 && this->value < 10000)
		this->text.setCharacterSize(32);
	else if (this->value > 10000 && this->value < 100000)
		this->text.setCharacterSize(28);
}

void Case::draw(sf::RenderWindow* window)
{
	window->draw(this->shape);
	window->draw(this->text);
}

sf::Color Case::getColor(unsigned int value)
{
	sf::Color color;
	switch (value)
	{
	case 2:
		color = sf::Color(149, 165, 166);
		break;
	case 4:
		color = sf::Color(127, 140, 141);
		break;
	case 8:
		color = sf::Color(241, 196, 15);
		break;
	case 16:
		color = sf::Color(243, 156, 18);
		break;
	case 32:
		color = sf::Color(230, 126, 34);
		break;
	case 64:
		color = sf::Color(211, 84, 0);
		break;
	case 128:
		color = sf::Color(231, 76, 60);
		break;
	case 256:
		color = sf::Color(192, 57, 43);
		break;
	case 512:
		color = sf::Color(44, 62, 80);
		break;
	case 1024:
		color = sf::Color(52, 73, 94);
		break;
	case 2048:
		color = sf::Color(41, 128, 185);
		break;
	case 4096:
		color = sf::Color(52, 152, 219);
		break;
	case 8192:
		color = sf::Color(39, 174, 96);
		break;
	case 16384:
		color = sf::Color(46, 204, 113);
		break;
	case 32768:
		color = sf::Color(155, 89, 182);
		break;
	case 65536:
		color = sf::Color(142, 68, 173);
		break;
	default:
		std::cout << "ERROR::VALUE DOESNT EXIST : " << value << std::endl;
		color = sf::Color(205, 193, 180);
		break;
	}

	return color;
}
